#include <Arduino.h>
#include <Game.h>
#include <Player.h>
#include <PlayerBrain.h>
#include <Renderer.h>
#include <UserControl.h>

// Pins configuration ESP8266
const byte sclk_pin = 2;    // D4
const byte din_pin = 16;    // D0
const byte dc_pin = 14;     // D5
const byte cs_pin = 12;     // D6
const byte rst_pin = 15;    // D8
const byte button_a_pin = 4;
const byte button_b_pin = 5;
const byte button_c_pin = 13;


Game * game = new Game();
Renderer * renderer = new Renderer(sclk_pin, din_pin, dc_pin, cs_pin, rst_pin);
UserControl * user_control = new UserControl(button_a_pin, button_b_pin, button_c_pin);

void setup() {

    Serial.begin(115200);
    Serial.println("Software started");

    renderer->begin();
    renderer->clear();
    delay(3000);

    game->initPlayers(2);
    // GAME_STATE_DEFINE_FIRST_PLAYER / GAME_STATE_CHARGE / GAME_STATE_DISCHARGE
    game->setGameState(GAME_STATE_DISCHARGE);
    renderer->playersPayementScreen(game);


    
}


void loop() {

}
