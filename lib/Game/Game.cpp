#include "Game.h"



Game::Game() {
    this->tokens = 21;
    this->combination_game_rules = CombinationGameRules::getInstance();
    this->round_start_callback = NULL;
}


void Game::initPlayers(int players_count) {
    this->players_count = players_count;
    this->players = new Player[players_count];
    for (int i = 0; i < this->players_count; i++) {
        this->players[i].setGame(this);
        this->players[i].setPlayerIndexInGame(i);
    }
}

void Game::triggerRoundStartEvent() {
    if (this->round_start_callback) {
        this->round_start_callback(this);
    }
}

void Game::onRoundStartEvent(GameEventCallbackFunction round_start_callback) {
    this->round_start_callback = round_start_callback;
}

void Game::triggerDefineFirstFinishedEvent() {
    if (this->define_first_finished_callback) {
        this->define_first_finished_callback(this);
    }
}

void Game::onDefineFirstFinishedEvent(GameEventCallbackFunction define_first_finished_callback) {
    this->define_first_finished_callback = define_first_finished_callback;
}

void Game::triggerChargeFinishedEvent() {
    if (this->charge_finished_callback) {
        this->charge_finished_callback(this);
    }
}

void Game::onChargeFinishedEvent(GameEventCallbackFunction charge_finished_callback) {
    this->charge_finished_callback = charge_finished_callback;
}

void Game::triggerDischargeFinishedEvent() {
    if (this->discharge_finished_callback) {
        this->discharge_finished_callback(this);
    }
}

void Game::onDischargeFinishedEvent(GameEventCallbackFunction discharge_finished_callback) {
    this->discharge_finished_callback = discharge_finished_callback;
}

void Game::triggerEqualityResolutionStartedEvent() {
    if (this->equality_resolution_started_callback) {
        this->equality_resolution_started_callback(this);
    }
}

void Game::onEqualityResolutionStartedEvent(GameEventCallbackFunction equality_resolution_started_callback) {
    this->equality_resolution_started_callback = equality_resolution_started_callback;
}

//
void Game::triggerPaymentEvent() {
    if (this->payment_callback) {
        this->payment_callback(this);
    }
}

void Game::onPaymentEvent(GameEventCallbackFunction payment_callback) {
    this->payment_callback = payment_callback;
}

Player * Game::getPlayerByIndex(int index) {
    return &this->players[index];
}

int Game::getPlayersCount() {
    return this->players_count;
}

Dice * Game::getDiceByIndex(int index) {
    return &this->dices[index];
}

void Game::rollDices(boolean dice_1, boolean dice_2, boolean dice_3) {
    if (dice_1) {
        this->dices[0].roll();
    }
    if (dice_2) {
        this->dices[1].roll();
    }
    if (dice_3) {
        this->dices[2].roll();
    }
}


void Game::initPlayersRound() {
    for (int i = 0; i < this->players_count; i++) {
        this->getPlayerByIndex(i)->initPlayerBeforeRound();
    }
}

int Game::findMaxScore(int score_type) {
    int max_score = -1;
    int absolute_max_score = -1;

    int player_score;
    int absolute_player_score;
    for (int i = 0; i < this->players_count; i++) {
        player_score = this->getPlayerByIndex(i)->getScore(score_type);
        if (player_score == 0) {
            continue;
        }
        absolute_player_score = this->absoluteScore(player_score);
        if (absolute_player_score > absolute_max_score) {
            absolute_max_score = absolute_player_score;
            max_score = player_score;
        }
    }
    return max_score;
}


int Game::findMinScore(int score_type) {
    int min_score = 10000;
    int absolute_min_score = 10000;

    int player_score;
    int absolute_player_score;
    for (int i = 0; i < this->players_count; i++) {
        player_score = this->getPlayerByIndex(i)->getScore(score_type);
        if (player_score == 0) {
            continue;
        }
        absolute_player_score = this->absoluteScore(player_score);
        if (absolute_player_score < absolute_min_score) {
            absolute_min_score = absolute_player_score;
            min_score = player_score;
        }
    }
    return min_score;
}


int Game::findRoundWinnerPlayerIndex() {
    return this->findWinnerPlayerIndex(Player::ROUND_SCORE);
}


int Game::findWinnerPlayerIndex(int score_type) {

    int max_score = this->findMaxScore(score_type);
    int max_player_index = -1;
    int same_score_count = 0;
    for (int i = 0; i < this->players_count; i++) {
        if (this->absoluteScore(this->getPlayerByIndex(i)->getScore(score_type)) == this->absoluteScore(max_score)) {
            max_player_index = i;
            same_score_count += 1;
        }
    }

    if (same_score_count > 1) {
        max_player_index = -1;
    }

    return max_player_index;
}


int Game::findRoundLoserPlayerIndex() {
    return this->findLoserPlayerIndex(Player::ROUND_SCORE);
}

int Game::findLoserPlayerIndex(int score_type) {

    int min_score = this->findMinScore(score_type);
    int min_player_index = -1;
    int same_score_count = 0;
    for (int i = 0; i < this->players_count; i++) {
        if (this->absoluteScore(this->getPlayerByIndex(i)->getScore(score_type)) == this->absoluteScore(min_score)) {
            min_player_index = i;
            same_score_count += 1;
        }
    }

    if (same_score_count > 1) {
        min_player_index = -1;
    }

    return min_player_index;
}


int Game::absoluteScore(int score) {
    return this->combination_game_rules->absoluteScore(score);
}

int Game::findFirstPlayerIndex() {
    int result = -1;
    for (int i = 0; i < this->players_count; i++) {
        if (this->getPlayerByIndex(i)->getIsFirstInRound()) {
            result = i;
        }
    }
    return result;
}

int Game::countPlayersNeedsEqualityResolution() {
    int result = 0;
    for (int i = 0; i < this->players_count; i++) {
        if (this->getPlayerByIndex(i)->getNeedsEqualityResolution()) {
            result += 1;
        }
    }
    return result;
}

int Game::countWinnersPlayers() {
    int result = 0;
    for (int i = 0; i < this->players_count; i++) {
        if (this->getPlayerByIndex(i)->getGameWon()) {
            result += 1;
        }
    }
    return result;
}

/**
 * Main game Quick and dirty implementation
 */

void Game::start() {

    this->gameStepDetermineWhoStarts();
    this->triggerDefineFirstFinishedEvent();

    this->gameStepCharge();
    this->triggerChargeFinishedEvent();
    
    this->gameStepDischarge();
    this->triggerDischargeFinishedEvent();
}


void Game::gameStepDetermineWhoStarts() {
    this->setGameState(GAME_STATE_DEFINE_FIRST_PLAYER);
    this->initPlayersRound();

    for (int i = 0; i < this->players_count; i++) {
        this->getPlayerByIndex(i)->play();
    }

    int winner_player_index = this->findRoundWinnerPlayerIndex();
    if (winner_player_index == -1) {
        int max_score = this->findMaxScore(Player::ROUND_SCORE);
        winner_player_index = this->gameStepEqualityResolution(max_score);
    }
    this->getPlayerByIndex(winner_player_index)->setIsFirstInRound(true);
}


void Game::gameStepCharge() {
    this->setGameState(GAME_STATE_CHARGE);
    while(this->tokens > 0) {
        this->triggerRoundStartEvent();
        this->gameStepRoundCharge();
    }

    for (int i = 0; i < this->players_count; i++) {
        if (this->getPlayerByIndex(i)->getTokens() == 0) {
            this->getPlayerByIndex(i)->initPlayerBeforeRound();
            this->getPlayerByIndex(i)->setGameWon(true);
        }
    }
}


void Game::gameStepRoundCharge() {

    this->initPlayersRound();
    
    int first_player_index = this->findFirstPlayerIndex();
    for (int i = 0; i < this->players_count; i++) {
        this->getPlayerByIndex((i + first_player_index) % this->players_count)->play();
    }

    int min_score = this->findMinScore(Player::ROUND_SCORE);
    int loser_player_index = this->findRoundLoserPlayerIndex();
    if (loser_player_index == -1) {
        loser_player_index = this->gameStepEqualityResolution(min_score);
    }

    int tokens_to_pay = this->combination_game_rules->getTokensCountByScore(this->findMaxScore(Player::ROUND_SCORE));

    if (this->tokens >= tokens_to_pay) {
        this->tokens -= tokens_to_pay;
    } else {
        tokens_to_pay = this->tokens;
        this->tokens = 0;
    }

    this->getPlayerByIndex(first_player_index)->setIsFirstInRound(false);
    this->getPlayerByIndex(loser_player_index)->setIsFirstInRound(true);

    this->payment_player_index_to = loser_player_index;
    this->payment_tokens_amount = tokens_to_pay;
    this->processPayment();

}


void Game::gameStepDischarge() {
    this->setGameState(GAME_STATE_DISCHARGE);
    while(this->players_count - this->countWinnersPlayers() > 1) {
        this->triggerRoundStartEvent();
        this->gameStepRoundDischarge();
    }
}


void Game::gameStepRoundDischarge() {

    this->initPlayersRound();
    
    int first_player_index = this->findFirstPlayerIndex();

    for (int i = 0; i < this->players_count; i++) {
        if (this->getPlayerByIndex((i + first_player_index) % this->players_count)->getGameWon()) {
            continue;
        }
        this->getPlayerByIndex((i + first_player_index) % this->players_count)->play();
    }

    int min_score = this->findMinScore(Player::ROUND_SCORE);
    int max_score = this->findMaxScore(Player::ROUND_SCORE);

    int loser_player_index = this->findRoundLoserPlayerIndex();
    if (loser_player_index == -1) {
        loser_player_index = this->gameStepEqualityResolution(min_score);
    }

    int winner_player_index = this->findRoundWinnerPlayerIndex();
    if (winner_player_index == -1) {
        winner_player_index = this->gameStepEqualityResolution(max_score);
    }

    int tokens_to_pay = this->combination_game_rules->getTokensCountByScore(
        this->getPlayerByIndex(winner_player_index)->getRoundScore()
    );

    if (this->getPlayerByIndex(winner_player_index)->getTokens() <= tokens_to_pay) {
        tokens_to_pay = this->getPlayerByIndex(winner_player_index)->getTokens();
        this->getPlayerByIndex(winner_player_index)->setGameWon(true);
    }

    this->payment_player_index_from = winner_player_index;
    this->payment_player_index_to = loser_player_index;
    this->payment_tokens_amount = tokens_to_pay;
    this->processPayment();

    this->getPlayerByIndex(first_player_index)->setIsFirstInRound(false);
    this->getPlayerByIndex(loser_player_index)->setIsFirstInRound(true);

}


int Game::gameStepEqualityResolution(int score_to_resolve) {
     // Multiple winners found

    this->triggerEqualityResolutionStartedEvent();

    // Set all users that needs in equality resolution
    for (int i = 0; i < this->players_count; i++) {
        if (this->getPlayerByIndex(i)->getRoundScore() == score_to_resolve) {
            this->getPlayerByIndex(i)->setNeedsEqualityResolution(true);
        }
    }

    // while remaning needs_equality resolution
    while(this->countPlayersNeedsEqualityResolution() > 1) {
        for (int i = 0; i < this->players_count; i++) {
            if (this->getPlayerByIndex(i)->getNeedsEqualityResolution()) {
                this->getPlayerByIndex(i)->play();
            }
        }

        // Remove needsEqualityResolution on all losed players
        int max_absolute_equality_resolution_score 
            = this->absoluteScore(this->findMaxScore(Player::EQUALITY_RESOLUTION_SCORE));
        for (int i = 0; i < this->players_count; i++) {
            if (
                this->getPlayerByIndex(i)->getNeedsEqualityResolution()
                && this->absoluteScore(this->getPlayerByIndex(i)->getScore(Player::EQUALITY_RESOLUTION_SCORE))
                    < max_absolute_equality_resolution_score
            ) {
                this->getPlayerByIndex(i)->setNeedsEqualityResolution(false);
                this->getPlayerByIndex(i)->setEqualityResolutionScore(0);
            }
        }
    }

    int equality_winner_player_index = this->findWinnerPlayerIndex(Player::EQUALITY_RESOLUTION_SCORE);
    this->getPlayerByIndex(equality_winner_player_index)->setEqualityResolutionScore(0);
    this->getPlayerByIndex(equality_winner_player_index)->setNeedsEqualityResolution(false);
    
    return equality_winner_player_index;
}


int Game::getGameState() {
    return this->game_state;
}


void Game::setGameState(int game_state) {
    this->game_state = game_state;
}

boolean Game::assertGameStateEquals(int game_state) {
    return (this->getGameState() == game_state);
}

int Game::getTokens() {
    return this->tokens;
}


void Game::setTokens(int tokens) {
    this->tokens = tokens;
}

int Game::getNotYetPlayedPlayersCount() {
    int count = 0;
    for (int i = 0; i < this->players_count; i++) {
        if (
            (!this->getPlayerByIndex(i)->getRoundPlayed())
            && (!this->getPlayerByIndex(i)->getGameWon())
        ) {
            count ++;
        }
    }
    return count;
}

void Game::processPayment() {
    if (this->getGameState() == GAME_STATE_CHARGE) {
        if (
            this->payment_tokens_amount != -1
            && this->payment_player_index_to != -1
        ) {
            this->getPlayerByIndex(this->payment_player_index_to)->setTokens(
                this->getPlayerByIndex(this->payment_player_index_to)->getTokens() + this->payment_tokens_amount
            );
        }
    } else if (this->getGameState() == GAME_STATE_DISCHARGE) {
        if (
            this->payment_tokens_amount != -1
            && this->payment_player_index_to != -1
            && this->payment_player_index_from != -1
        ) {
            this->getPlayerByIndex(this->payment_player_index_from)->setTokens(
                this->getPlayerByIndex(this->payment_player_index_from)->getTokens() - this->payment_tokens_amount
            );

            this->getPlayerByIndex(this->payment_player_index_to)->setTokens(
                this->getPlayerByIndex(this->payment_player_index_to)->getTokens() + this->payment_tokens_amount
            );
        }
    }
    this->triggerPaymentEvent();
    this->resetPayment();
}


void Game::resetPayment() {
    this->payment_tokens_amount = -1;
    this->payment_player_index_to = -1;
    this->payment_player_index_from = 1;
}