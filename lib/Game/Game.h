#ifndef Game_h
#define Game_h

#include <Arduino.h>
#include "../Dice/Dice.h"
#include "../Player/Player.h"
#include "../CombinationGameRules/CombinationGameRules.h"


#define GAME_STATE_DEFINE_FIRST_PLAYER 1
#define GAME_STATE_CHARGE 2
#define GAME_STATE_DISCHARGE 3

class Game {
    
    private:
        int absoluteScore(int score);

    public:
        // @todo Try implementing event for RoundStart
        typedef std::function<void(Game * game)> GameEventCallbackFunction;

        GameEventCallbackFunction round_start_callback;
        GameEventCallbackFunction define_first_finished_callback;
        GameEventCallbackFunction charge_finished_callback;
        GameEventCallbackFunction discharge_finished_callback;
        GameEventCallbackFunction equality_resolution_started_callback;
        GameEventCallbackFunction payment_callback;

        CombinationGameRules * combination_game_rules;

        Dice dices[3];

        int tokens;

        int players_count;
        Player * players;
        int round_roll_dices_count;

        int game_state;
        boolean resolve_players_equality;

        // Transaction for payments @todo
        int payment_tokens_amount = -1;
        int payment_player_index_to = -1;
        int payment_player_index_from = -1;

        void processPayment();
        void resetPayment();

        Game();

        void start();
        void gameStepDetermineWhoStarts();
        void gameStepCharge();
        void gameStepRoundCharge();
        void gameStepDischarge();
        void gameStepRoundDischarge();
        int  gameStepEqualityResolution(int score_to_resolve);

        int countWinnersPlayers();
        void initPlayers(int players_count);
        void initPlayersRound();
        void rollDices(boolean dice_1, boolean dice_2, boolean dice_3);
        int  findFirstPlayerIndex();
        int  countPlayersNeedsEqualityResolution();

        int findMaxScore(int score_type);
        int findMinScore(int score_type);

        int findRoundWinnerPlayerIndex();
        int findRoundLoserPlayerIndex();

        int findWinnerPlayerIndex(int score_type);
        int findLoserPlayerIndex(int score_type);
    
        int getPlayersCount();
        int getNotYetPlayedPlayersCount();
        Player * getPlayerByIndex(int index);
        Dice * getDiceByIndex(int index);

        int getGameState();
        void setGameState(int game_state);
        boolean assertGameStateEquals(int game_state);

        int getTokens();
        void setTokens(int tokens);

        // Events declaration
        void triggerRoundStartEvent();
        void onRoundStartEvent(GameEventCallbackFunction round_start_callback);

        void triggerDefineFirstFinishedEvent();
        void onDefineFirstFinishedEvent(GameEventCallbackFunction define_first_finished_callback);

        void triggerChargeFinishedEvent();
        void onChargeFinishedEvent(GameEventCallbackFunction charge_finished_callback);

        void triggerDischargeFinishedEvent();
        void onDischargeFinishedEvent(GameEventCallbackFunction discharge_finished_callback);

        void triggerEqualityResolutionStartedEvent();
        void onEqualityResolutionStartedEvent(GameEventCallbackFunction equality_resolution_started_callback);

        void triggerPaymentEvent();
        void onPaymentEvent(GameEventCallbackFunction payment_callback);

};

#endif