#include "PlayerBrain.h"


PlayerBrain::PlayerBrain() {
    this->irational_factor = 0;
    this->improvements_dice_score = new ImprovementsDiceScore();
}


void PlayerBrain::hydrate() {

    this->resetDecisionResult();

    // get active users count
    this->setPlayersCount(this->game->getPlayersCount());

    // get count of remaining users to play in round
    this->setNotYetPlayedPlayersCount(this->game->getNotYetPlayedPlayersCount());

    // get best score
    this->setBestScore(this->game->findMaxScore(Player::ROUND_SCORE));

    // get my launches
    this->setLaunchesCount(this->player->getRoundRollDicesCount());

    // player_current_score
    this->setPlayerCurrentScore(player->getRoundScore());

}

void PlayerBrain::play(Game * game, Player * player) {

    // Need to check if is equality resolution
    for(int i = 0; i < 3; i ++) {
        this->hydrate();
        this->makeDecision();
        if (!this->getRollDices()) {
            return;
        }
        this->playerRollDices();
    }
}


void PlayerBrain::makeDecision() {

    if (this->getLaunchesCount() == 0) {
        this->roll_dice_1 = true;
        this->roll_dice_2 = true;
        this->roll_dice_3 = true;
        this->setRollDices(true);
        return;
    }

    if (this->getIsFirstInRound()) {
        if (this->isAcceptableFirstScore()) {
            return;
        } else {
            // @todo try to improve score
            // ...
        }
    }


}


void PlayerBrain::playerRollDices() {
    if (this->roll_dices) {
        this->player->rollDices(
            this->roll_dice_1,
            this->roll_dice_2,
            this->roll_dice_3
        );
    }
}


void PlayerBrain::setIrationalFactor(int irational_factor) {
    this->irational_factor = irational_factor;
}

int PlayerBrain::getIrationalFactor() {
    return this->irational_factor;
}

int PlayerBrain::getNotYetPlayedPlayersCount() {
    return this->not_yet_played_players_count;
}

void PlayerBrain::setNotYetPlayedPlayersCount(int not_yet_played_players_count) {
    this->not_yet_played_players_count = not_yet_played_players_count;
}

int PlayerBrain::getPlayersCount() {
    return this->players_count;
}

void PlayerBrain::setPlayersCount(int players_count) {
    this->players_count = players_count;
}

int PlayerBrain::getBestScore() {
    return this->best_score;
}

void PlayerBrain::setBestScore(int best_score) {
    this->best_score = best_score;
}

int PlayerBrain::getLaunchesCount() {
    return this->launches_count;
}

void PlayerBrain::setLaunchesCount(int launches_count) {
    this->launches_count = launches_count;
}

int PlayerBrain::getPlayerCurrentScore() {
    return this->player_current_score;
}

void PlayerBrain::setPlayerCurrentScore(int player_current_score) {
    this->player_current_score = player_current_score;
}

void PlayerBrain::setRollDices(bool roll_dices) {
    this->roll_dices = roll_dices;
}

bool PlayerBrain::getRollDices() {
    return this->roll_dices;
}

bool PlayerBrain::getIsFirstInRound() {
    return (this->getPlayersCount() - this->getNotYetPlayedPlayersCount()) == 0;
}


bool PlayerBrain::isAcceptableFirstScore() {
    int absolute_score = this->game->combination_game_rules->absoluteScore(this->getPlayerCurrentScore());
    if (absolute_score > this->MEDIAN_SCORE) {
        return true;
    }
    return false;
}

void PlayerBrain::resetDecisionResult() {
    this->roll_dice_1 = false;
    this->roll_dice_2 = false;
    this->roll_dice_3 = false;
    this->roll_dices = false;
}


void PlayerBrain::possibleImprovements(int dice_1_value, int dice_2_value, int dice_3_value) {

    // Should return: returns Array of possible best improvments, with probability score
    // Maybe to preserve memory should be limited to 3 items best propositions
    /*
    [
        {
            target improvment score: INT
            tokens: score INT
            probability: fload 0 < 1
            dice_1 Bool
            dice_2 Bool
            dice_3 Bool
        }
    ]
    */
}


void PlayerBrain::resetPossibleImprovements() {
    for(int i = 0; i < IMPROVEMENT_COUNT; i++) {
        this->improvement_list[i].target_score = 0;
    }
}