#ifndef PlayerBrain_h
#define PlayerBrain_h

#include <Arduino.h>

#include "../Dice/Dice.h"
#include "../Game/Game.h"
#include "../Player/Player.h"
#include "../CombinationGameRules/CombinationGameRules.h"
#include "../ImprovementsDiceScore/ImprovementsDiceScore.h"

/**
 * About PlayerBrain class
 * 
 * For now stateless decisions are made. Means on each decision evaluation
 * all params ares setted
 * 
 * Simplify way to test
 * 
 * sequence:
 * - setting params in class members vars
 * - making decision
 * - decision is stored in class members vars
 * - playing (apply decision to the game)
 * - reseting all params
 * 
 */

class PlayerBrain {
    
    public:


        ImprovementsDiceScore * improvements_dice_score;

        struct ScoreImprovement {
            int target_score;
            int target_tokens;
            float probability;
            bool dice_1;
            bool dice_2;
            bool dice_3;
        };

        const static int IMPROVEMENT_COUNT = 10;

        ScoreImprovement improvement_list[IMPROVEMENT_COUNT];

        // Result
        bool roll_dices;
        bool roll_dice_1;
        bool roll_dice_2;
        bool roll_dice_3;

        PlayerBrain();

        void setGame(Game * game);
        void setPlayer(Player * player);
        void hydrate();
        void resetDecisionResult();


        void makeDecision();
        void play(Game * game, Player * player);

        void setIrationalFactor(int irational_factor);
        int getIrationalFactor();


        int getNotYetPlayedPlayersCount();
        void setNotYetPlayedPlayersCount(int not_yet_played_players_count);

        int getPlayersCount();
        void setPlayersCount(int players_count);

        int getBestScore();
        void setBestScore(int best_score);

        int getLaunchesCount();
        void setLaunchesCount(int launches_count);

        int getPlayerCurrentScore();
        void setPlayerCurrentScore(int player_current_score);

        void setRollDices(bool roll_dices);
        bool getRollDices();

        bool getIsFirstInRound();

        bool isAcceptableFirstScore();

        void playerRollDices();

        void possibleImprovements(int dice_1_value, int dice_2_value, int dice_3_value);
        void resetPossibleImprovements();

    private:

        Game * game;
        Player * player;

        int irational_factor;

        // get active users count
        int players_count;

        int not_yet_played_players_count;

        int best_score;

        int launches_count;

        int player_current_score;


        const int MEDIAN_SCORE = 553;


};

#endif