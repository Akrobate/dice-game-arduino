#include "Dice.h"

#define UNIT_TEST 1

Dice::Dice() {
    this->reset();
}

void Dice::roll() {
    #ifdef UNIT_TEST
        this->value = (rand() % 6) + 1;
    #else
        this->value = random(1, 7);
    #endif
}

void Dice::reset() {
    this->value = 0;
}

int Dice::getValue() {
    return this->value;
}