#ifndef Dice_h
#define Dice_h

#include <Arduino.h>

class Dice {
    
    public:
        Dice();
        void roll();
        void reset();
        int getValue();
    private:
        int value;
};

#endif