#include "CombinationGameRules.h"

CombinationGameRules *CombinationGameRules::instance = nullptr;


CombinationGameRules * CombinationGameRules::getInstance() {
    if (!CombinationGameRules::instance) {
        CombinationGameRules::instance = new CombinationGameRules();
    }
    return CombinationGameRules::instance;
}

CombinationGameRules::CombinationGameRules() {

    this->combinations[0] = {1, 421, 10, this->COMBINATION_TYPE_421};
    this->combinations[1] = {2, 111, 7, this->COMBINATION_TYPE_TRIPLE_ONE};
    this->combinations[2] = {3, 611, 6, this->COMBINATION_TYPE_DOUBLE_ONE};
    this->combinations[3] = {4, 666, 6, this->COMBINATION_TYPE_SAME_VALUE};

    this->combinations[4] = {5, 511, 5, this->COMBINATION_TYPE_DOUBLE_ONE};
    this->combinations[5] = {6, 555, 5, this->COMBINATION_TYPE_SAME_VALUE};

    this->combinations[6] = {7, 411, 4, this->COMBINATION_TYPE_DOUBLE_ONE};
    this->combinations[7] = {8, 444, 4, this->COMBINATION_TYPE_SAME_VALUE};
    
    this->combinations[8] = {9, 311, 3, this->COMBINATION_TYPE_DOUBLE_ONE};
    this->combinations[9] = {10, 333, 3, this->COMBINATION_TYPE_SAME_VALUE};

    this->combinations[10] = {11, 211, 2, this->COMBINATION_TYPE_DOUBLE_ONE};
    this->combinations[11] = {12, 222, 2, this->COMBINATION_TYPE_SAME_VALUE};

    this->combinations[12] = {13, 654, 2, this->COMBINATION_TYPE_SUITE};
    this->combinations[13] = {14, 543, 2, this->COMBINATION_TYPE_SUITE};
    this->combinations[14] = {15, 432, 2, this->COMBINATION_TYPE_SUITE};
    this->combinations[15] = {16, 321, 2, this->COMBINATION_TYPE_SUITE};
}


int CombinationGameRules::getTokensCountByScore(int value) {
    int result = 1;
    int index = this->findCombinationIndexByScore(value);
    if (index > -1) {
        result = this->getCombinationByIndex(index).tokens;
    }
    return result;
}


CombinationGameRules::Combination CombinationGameRules::getCombinationByIndex(int index) {
    return this->combinations[index];
}


int CombinationGameRules::findCombinationIndexByScore(int value) {
    int result = -1;
    for(int i = 0; i < 16; i++) {
        if (this->getCombinationByIndex(i).value == value) {
            result = i;
            break;
        }
    }
    return result;
}


int CombinationGameRules::absoluteScore(int score) {
    int absolute_score = score;
    int combination_index = this->findCombinationIndexByScore(score);
    if (combination_index >= 0) {
        absolute_score = 700 + (16 - combination_index);
    }
    return absolute_score;
}


int CombinationGameRules::getCombinationType(int score) {
    int result = this->COMBINATION_TYPE_SIMPLE_SCORE;
    int combination_index = this->findCombinationIndexByScore(score);
    if (combination_index >= 0) {
        result = this->getCombinationByIndex(combination_index).type;
    }
    return result;
}



int CombinationGameRules::dicesValuesToInt(int dice_1_value, int dice_2_value, int dice_3_value) {
    int values_list[] = {dice_1_value, dice_2_value, dice_3_value};
    int tmp;
    for (int j = 0; j < 3; j++) {
        for (int i = 0; i < 2; i++) {
            if (values_list[i+1] > values_list[i]) {
                tmp = values_list[i];
                values_list[i] = values_list[i+1];
                values_list[i+1] = tmp;
            }
        }
    }
    return (values_list[0] * 100) + (values_list[1] * 10) + (values_list[2] * 1);
}

