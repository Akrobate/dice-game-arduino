#ifndef CombinationGameRules_h
#define CombinationGameRules_h

#include <Arduino.h>

class CombinationGameRules {
    
    public:

        static CombinationGameRules * instance;

        static CombinationGameRules *getInstance();

        struct Combination {
            int order;
            int value;
            int tokens;
            int type;
        };

        const int COMBINATION_TYPE_SUITE = 1;
        const int COMBINATION_TYPE_DOUBLE_ONE = 2;
        const int COMBINATION_TYPE_TRIPLE_ONE = 3;
        const int COMBINATION_TYPE_421 = 4;
        const int COMBINATION_TYPE_SAME_VALUE = 5;
        const int COMBINATION_TYPE_SIMPLE_SCORE = 6;

        Combination combinations[16];

        

        int getTokensCountByScore(int value);
        Combination getCombinationByIndex(int index);
        int findCombinationIndexByScore(int value);
        int absoluteScore(int score);
        int getCombinationType(int score);
        int dicesValuesToInt(int dice_1_value, int dice_2_value, int dice_3_value);

    private:
        CombinationGameRules();
};

#endif