#include "./UserControl.h"

UserControl::UserControl(byte button_a_pin, byte button_b_pin, byte button_c_pin) {
    pinMode(button_a_pin, INPUT_PULLUP);
    pinMode(button_b_pin, INPUT_PULLUP);
    pinMode(button_c_pin, INPUT_PULLUP);

    this->button_a = new Debounce(button_a_pin, DEBOUNCE_MILLIS);
    this->button_b = new Debounce(button_b_pin, DEBOUNCE_MILLIS);
    this->button_c = new Debounce(button_c_pin, DEBOUNCE_MILLIS);
}


void UserControl::init() {

}


void UserControl::update() {

}


boolean UserControl::valueChangedA() {
    byte new_value = this->button_a->read();
    if (this->button_a_value != new_value) {
        this->button_a_value = new_value;
        return true;
    }
    return false;
}


boolean UserControl::valueChangedB() {
    byte new_value = this->button_b->read();
    if (this->button_b_value != new_value) {
        this->button_b_value = new_value;
        return true;
    }
    return false;
}


boolean UserControl::valueChangedC() {
    byte new_value = this->button_c->read();
    if (this->button_c_value != new_value) {
        this->button_c_value = new_value;
        return true;
    }
    return false;
}