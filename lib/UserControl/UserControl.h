#ifndef UserControl_h
#define UserControl_h

#include <Debounce.h>
#include <Arduino.h>

#define DEBOUNCE_MILLIS     100

class UserControl {
    
    public:
        UserControl(byte button_a_pin, byte button_b_pin, byte button_c_pin);

        void init();
        void update();
        byte button_a_value;
        byte button_b_value;
        byte button_c_value;

        boolean valueChangedA();
        boolean valueChangedB();
        boolean valueChangedC();

    private:

        Debounce * button_a;
        Debounce * button_b;
        Debounce * button_c;

};

#endif