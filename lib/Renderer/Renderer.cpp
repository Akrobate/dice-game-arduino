#include "Renderer.h"

Renderer::Renderer(
    byte sclk_pin,
    byte din_pin,
    byte dc_pin,
    byte cs_pin,
    byte rst_pin
) {
    this->_lcd = new Adafruit_PCD8544(sclk_pin, din_pin, dc_pin, cs_pin, rst_pin);
}

void Renderer::begin() {
    this->_lcd->begin();
    this->_lcd->setContrast(50);
    this->_lcd->setRotation(2); // Flip screen up / down
    this->_lcd->clearDisplay();
    this->_lcd->display();
}

void Renderer::clear() {
    this->_lcd->clearDisplay();
    this->_lcd->display();
}

void Renderer::setCursor(uint8_t x, uint8_t y) {
    this->_lcd->setCursor(x, y);
}

void Renderer::print(const char* str) {
    this->_lcd->print(str);
    this->_lcd->display();
}



void Renderer::splashScreen() {
    this->clear();
    this->setCursor(0, 0);
    this->print("Welcome screen");
}


void Renderer::mainMenuScreen() {
    this->clear();
    this->setCursor(0, 0);
    this->print("Main menu screen");
}

void Renderer::roundRepportScreen(Game * game) {
    this->clear();
    this->setCursor(0, 0);
    this->print("roundRepportScreen");
}

void Renderer::equalityResolutionScreen() {
    this->clear();
    this->setCursor(0, 0);
    this->print("equalityResolution");
}


void Renderer::playersPayementScreen(Game * game) {
    if (game->getGameState() == GAME_STATE_CHARGE) {
        this->playersChargePayementScreen(game);
    } else if (game->getGameState() == GAME_STATE_DISCHARGE) {
        this->playersDischargePayementScreen(game);
    }
}



void Renderer::playersDischargePayementScreen(Game * game) {
    this->clear();
    this->setCursor(0, 0);

    int payment_tokens_amount = game->payment_tokens_amount;
    int payment_player_index_to = game->payment_player_index_to;
    int payment_player_index_from = game->payment_player_index_from;

    this->print("PlayerPayementScreen");
    this->print("Payement DISCHARGE");
}


void Renderer::playersChargePayementScreen(Game * game) {
    this->clear();
    this->setCursor(0, 0);

    int payment_tokens_amount = game->payment_tokens_amount;
    int payment_player_index_to = game->payment_player_index_to;
    int payment_player_index_from = game->payment_player_index_from;

    this->print("PlayerPayementScreen");
    this->print("Payement CHARGE");
}



void Renderer::dicesLaunchingScreen(Game * game, Player * player) {
    this->clear();
    this->setCursor(0, 0);
    this->print("dicesLaunchingScreen");

}
