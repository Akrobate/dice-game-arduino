#ifndef Renderer_h
#define Renderer_h

#include <Arduino.h>
#include <Adafruit_PCD8544.h>
#include <Game.h>
#include <Player.h>


class Renderer {

    private:
        Adafruit_PCD8544 * _lcd;

    public:
        Renderer(
            byte sclk_pin,
            byte din_pin,
            byte dc_pin,
            byte cs_pin,
            byte rst_pin
        );

        void begin();
        void clear();
        void setCursor(uint8_t x, uint8_t y);
        void print(const char* str);

        void splashScreen();
        void mainMenuScreen();
        
        void equalityResolutionScreen();

        void roundRepportScreen(Game * game);
        void dicesLaunchingScreen(Game * game, Player * player);

        void playersPayementScreen(Game * game);
        void playersDischargePayementScreen(Game * game);
        void playersChargePayementScreen(Game * game);
        
        
};

#endif