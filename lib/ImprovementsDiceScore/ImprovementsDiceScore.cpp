#include "ImprovementsDiceScore.h"

ImprovementsDiceScore::ImprovementsDiceScore() {
    this->combination_game_rules = CombinationGameRules::getInstance();
}

void ImprovementsDiceScore::possibleImprovements(int dice_1_value, int dice_2_value, int dice_3_value) {

    this->resetPossibleImprovements();

    int score = this->combination_game_rules->dicesValuesToInt(dice_1_value, dice_2_value, dice_3_value);
    int combination_type = this->combination_game_rules->getCombinationType(score);

    const int COMBINATION_TYPE_SUITE = this->combination_game_rules->COMBINATION_TYPE_SUITE;
    const int COMBINATION_TYPE_DOUBLE_ONE = this->combination_game_rules->COMBINATION_TYPE_DOUBLE_ONE;
    const int COMBINATION_TYPE_TRIPLE_ONE = this->combination_game_rules->COMBINATION_TYPE_TRIPLE_ONE;
    const int COMBINATION_TYPE_421 = this->combination_game_rules->COMBINATION_TYPE_421;
    const int COMBINATION_TYPE_SAME_VALUE = this->combination_game_rules->COMBINATION_TYPE_SAME_VALUE;
    const int COMBINATION_TYPE_SIMPLE_SCORE = this->combination_game_rules->COMBINATION_TYPE_SIMPLE_SCORE;


    if (combination_type == COMBINATION_TYPE_SUITE) {

    }

    if (combination_type == COMBINATION_TYPE_DOUBLE_ONE) {

    }

    if (combination_type == COMBINATION_TYPE_TRIPLE_ONE) {

    }

    if (combination_type == COMBINATION_TYPE_421) {

    }

    if (combination_type == COMBINATION_TYPE_SAME_VALUE) {

    }

    if (combination_type == COMBINATION_TYPE_SIMPLE_SCORE) {

    }

    // Should return: returns Array of possible best improvments, with probability score
    // Maybe to preserve memory should be limited to 3 items best propositions
    /*
    [
        {
            target improvment score: INT
            tokens: score INT
            probability: fload 0 < 1
            dice_1 Bool
            dice_2 Bool
            dice_3 Bool
        }
    ]
    */
}

void ImprovementsDiceScore::resetPossibleImprovements() {
    for(int i = 0; i < IMPROVEMENT_COUNT; i++) {
        this->improvement_list[i].target_score = 0;
    }
}