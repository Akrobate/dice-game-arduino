#ifndef ImprovementsDiceScore_h
#define ImprovementsDiceScore_h

#include <Arduino.h>

#include "../Dice/Dice.h"
#include "../Game/Game.h"
#include "../Player/Player.h"
#include "../CombinationGameRules/CombinationGameRules.h"

/**
 * About ImprovementsDiceScore class
 * 
 * For now stateless decisions are made. Means on each decision evaluation
 * all params ares setted
 * 
 * Simplify way to test
 * 
 * sequence:
 * - setting params in class members vars
 * - making decision
 * - decision is stored in class members vars
 * - playing (apply decision to the game)
 * - reseting all params
 * 
 */

class ImprovementsDiceScore {
    
    public:

        CombinationGameRules * combination_game_rules;
        struct ScoreImprovement {
            int target_score;
            int target_tokens;
            float probability;
            bool dice_1;
            bool dice_2;
            bool dice_3;
        };

        const static int IMPROVEMENT_COUNT = 10;

        ScoreImprovement improvement_list[IMPROVEMENT_COUNT];

        ImprovementsDiceScore();
        void possibleImprovements(int dice_1_value, int dice_2_value, int dice_3_value);
        void resetPossibleImprovements();

    private:

};

#endif