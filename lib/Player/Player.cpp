#include "Player.h"
#include "../Game/Game.h"


Player::Player() {
    this->play_callback = NULL;

    this->is_first_in_round = false;
    
    this->tokens = 0;
    this->setGameWon(false);
    this->initPlayerBeforeRound();
}


void Player::play() {
    if (this->play_callback) {
        this->play_callback(this);
    }
}

void Player::initPlayerBeforeRound() {
    this->setRoundPlayed(false);
    this->setRoundScore(0);
    this->setEqualityResolutionScore(0);
    this->setNeedsEqualityResolution(false);

    this->round_roll_dices_count = 0;
}

void Player::onPlayEvent(PlayCallbackFunction play_callback) {
    this->play_callback = play_callback;
}


void Player::rollDices(boolean dice_1, boolean dice_2, boolean dice_3) {

    if (this->needs_equality_resolution) {
        this->game->rollDices(true, true, true);
        this->equality_resolution_score = this->game->combination_game_rules->dicesValuesToInt(
            this->getDiceValueByIndex(0),
            this->getDiceValueByIndex(1),
            this->getDiceValueByIndex(2)
        );
        return;
    }

    if (this->round_roll_dices_count == 0) {
        this->game->rollDices(true, true, true);
    } else {
        this->game->rollDices(dice_1, dice_2, dice_3);
    }
    this->round_roll_dices_count++;
    this->round_score = this->game->combination_game_rules->dicesValuesToInt(
        this->getDiceValueByIndex(0),
        this->getDiceValueByIndex(1),
        this->getDiceValueByIndex(2)
    );
}

int Player::getDiceValueByIndex(int index) {
    return this->game->dices[index].getValue();
}

void Player::setGame(Game * game) {
    this->game = game;
}

Game * Player::getGame() {
    return this->game;
}

void Player::setPlayerIndexInGame(int index) {
    this->player_index_in_game = index;
}

int Player::getPlayerIndexInGame() {
    return this->player_index_in_game;
}

void Player::setTokens(int tokens) {
    this->tokens = tokens;
}

int Player::getTokens() {
    return this->tokens;
}

int Player::getRoundScore() {
    return this->round_score;
}

int Player::getScore(int score_type) {
    int result = -1;
    if (Player::EQUALITY_RESOLUTION_SCORE == score_type) {
        result = this->equality_resolution_score;
    }
    if (Player::ROUND_SCORE == score_type) {
        result = this->round_score;
    }
    return result;
}

void Player::setRoundScore(int score) {
    this->round_score = score;
}

void Player::setEqualityResolutionScore(int score) {
    this->equality_resolution_score = score;
}

void Player::setRoundPlayed(boolean round_played) {
    this->round_played = round_played;
}

boolean Player::getRoundPlayed() {
    return this->round_played;
}

int Player::getRoundRollDicesCount() {
    return this->round_roll_dices_count;
}

void Player::setNeedsEqualityResolution(boolean needs_equality_resolution) {
    this->needs_equality_resolution = needs_equality_resolution;
}

boolean Player::getNeedsEqualityResolution() {
    return this->needs_equality_resolution;
}

boolean Player::getIsFirstInRound() {
    return this->is_first_in_round;
}

void Player::setIsFirstInRound(boolean is_first_in_round) {
    this->is_first_in_round = is_first_in_round;
}

boolean Player::getGameWon() {
    return this->game_won;
}

void Player::setGameWon(boolean game_won) {
    this->game_won = game_won;
}
