#ifndef Player_h
#define Player_h

#include <Arduino.h>

/**
 * About Player class
 * 
 * - Player has initial state with all stats setted to 0
 * - Player that has not played in round will have a score of 0
 * - When a player made a same score then anothe, needs_equality_resolution is setted true
 *   the equality_resolution_score will be used
 * 
 */

class Game;

class Player {
    
    public:

        static const int ROUND_SCORE = 1;
        static const int EQUALITY_RESOLUTION_SCORE = 2;

        typedef std::function<void(Player * player)> PlayCallbackFunction;

        int player_index_in_game;

        int tokens;
        boolean game_won;

        boolean needs_equality_resolution;
        int equality_resolution_score;

        boolean round_played;
        boolean is_first_in_round;
        int round_score;
        int round_roll_dices_count;

        PlayCallbackFunction play_callback;
        Game * game;


        Player();

        void play();
        void initPlayerBeforeRound();
        void onPlayEvent(PlayCallbackFunction play_callback);



        void setGame(Game * game);
        Game * getGame();

        void setPlayerIndexInGame(int index);
        int getPlayerIndexInGame();

        void setTokens(int tokens);
        int getTokens();

        int getDiceValueByIndex(int index);

        int getRoundScore();
        int getRoundRollDicesCount();
        void setRoundScore(int score);
        int getScore(int score_type);
        void setEqualityResolutionScore(int score);
        boolean getNeedsEqualityResolution();
        void setNeedsEqualityResolution(boolean needs_equality_resolution);
        boolean getIsFirstInRound();
        void setIsFirstInRound(boolean is_first_in_round);
        boolean getGameWon();
        void setGameWon(boolean game_won);

        void rollDices(boolean dice_1, boolean dice_2, boolean dice_3);

        void setRoundPlayed(boolean round_played);
        boolean getRoundPlayed();

    private:

};

#endif