const fs = require('fs')
const path = require('path');

async function saveData(filename, data, formated = false) {
    let string_json_data = '';
    if (formated) {
        string_json_data = JSON.stringify(data, null, 4);
    } else {
        string_json_data = JSON.stringify(data);
    }

    await fs.promises.writeFile(
        filename,
        string_json_data,
        {
            encoding: 'utf8',
        }
    );
}

async function loadData(filename) {
    const data = await fs.promises.readFile(
        filename,
        {
            encoding: 'utf8',
        }
    );
    return JSON.parse(data);
}


function fileExists(filename) {
    return fs.existsSync(filename)
}


function generateDataFilepath(filename) {
    return path.join(__dirname, '..', 'data', filename);
}

module.exports = {
    saveData,
    loadData,
    fileExists,
    generateDataFilepath,
}