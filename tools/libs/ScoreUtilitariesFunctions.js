function absoluteScore(combinations, score) {
    let absolute_score = score;
    const combination_index = combinations.findIndex((item) => item.score === score)
    if (combination_index >= 0) {
        absolute_score = 700 + (16 - combination_index);
    }
    return absolute_score;
}

function diceToIntValue(dice_1_value, dice_2_value, dice_3_value) {
    const values_list = [dice_1_value, dice_2_value, dice_3_value];
    let tmp;
    for (let j = 0; j < 3; j++) {
        for (let i = 0; i < 2; i++) {
            if (values_list[i+1] > values_list[i]) {
                tmp = values_list[i];
                values_list[i] = values_list[i+1];
                values_list[i+1] = tmp;
            }
        }
    }
    return (values_list[0] * 100) + (values_list[1] * 10) + (values_list[2] * 1);
}

function intScoreToFormatedIntScore(score) {
    return diceToIntValue(`${score}`[0], `${score}`[1], `${score}`[2])
}

module.exports = {
    absoluteScore,
    diceToIntValue,
    intScoreToFormatedIntScore,
}