const COMBINATION_TYPE = {
    SUITE: 1,
    DOUBLE_ONE: 2,
    TRIPLE_ONE: 3,
    '421': 4,
    SAME_VALUE: 5,
    SIMPLE_SCORE: 6,
}

const combinations = [
    {
        order: 1, 
        score: 421,
        tokens: 10,
        type: COMBINATION_TYPE['421'],
    },
    {
        order: 2,
        score: 111,
        tokens: 7,
        type: COMBINATION_TYPE.TRIPLE_ONE,
    },
    {
        order: 3,
        score: 611,
        tokens: 6,
        type: COMBINATION_TYPE.DOUBLE_ONE,
    },
    {
        order: 4,
        score: 666,
        tokens: 6,
        type: COMBINATION_TYPE.SAME_VALUE,
    },
    {
        order: 5,
        score: 511,
        tokens: 5,
        type: COMBINATION_TYPE.DOUBLE_ONE,
    },
    {
        order: 6,
        score: 555,
        tokens: 5,
        type: COMBINATION_TYPE.SAME_VALUE,
    },
    {
        order: 7,
        score: 411,
        tokens: 4,
        type: COMBINATION_TYPE.DOUBLE_ONE,
    },
    {
        order: 8,
        score: 444,
        tokens: 4,
        type: COMBINATION_TYPE.SAME_VALUE,
    },
    {
        order: 9,
        score: 311,
        tokens: 3,
        type: COMBINATION_TYPE.DOUBLE_ONE,
    },
    {
        order: 10,
        score: 333,
        tokens: 3,
        type: COMBINATION_TYPE.SAME_VALUE,
    },
    {
        order: 11,
        score: 211,
        tokens: 2,
        type: COMBINATION_TYPE.DOUBLE_ONE,
    },
    {
        order: 12,
        score: 222,
        tokens: 2,
        type: COMBINATION_TYPE.SAME_VALUE,
    },
    {
        order: 13,
        score: 654,
        tokens: 2,
        type: COMBINATION_TYPE.SUITE,
    },
    {
        order: 14,
        score: 543,
        tokens: 2,
        type: COMBINATION_TYPE.SUITE,
    },
    {
        order: 15,
        score: 432,
        tokens: 2,
        type: COMBINATION_TYPE.SUITE,
    },
    {
        order: 16,
        score: 321,
        tokens: 2,
        type: COMBINATION_TYPE.SUITE,
    },
];

module.exports = {
    combinations,
    COMBINATION_TYPE
}
