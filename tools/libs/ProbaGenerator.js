const {
    absoluteScore,
    diceToIntValue,
    intScoreToFormatedIntScore,
} = require('./ScoreUtilitariesFunctions');
const {
    combinations,
} = require('./CombinationGameRules');


function generateRandomDiceScore() {
    return Math.floor(Math.random() * 6) + 1;
}


function generateRandomScore() {
    const d1 = generateRandomDiceScore();
    const d2 = generateRandomDiceScore();
    const d3 = generateRandomDiceScore();
    return diceToIntValue(d1, d2, d3)
}


function generateAllPossibilities() {
    const result = [];
    for(let i = 1; i < 7; i++) {
        for(let j = 1; j < 7; j++) {
            for(let k = 1; k < 7; k++) {
                score = Number(`${i}${j}${k}`);
                if (result.find((item) => item === intScoreToFormatedIntScore(score)) === undefined) {
                    result.push(intScoreToFormatedIntScore(score));
                }
            }
        }
    }
    return result;
}


function generateWinProba(score, epochs = 100000) {
    let win = 0;
    const absolute_score = absoluteScore(combinations, score);
    for(let i = 0; i < epochs; i++) {
        if (absolute_score > absoluteScore(combinations, generateRandomScore())) {
            win++; 
        }
    }
    return win / epochs;
}


function generateWinProbaAllScores(epochs = 100000) {
    const all_scores = generateAllPossibilities();
    return all_scores.map((score) => ({
        score,
        proba: generateWinProba(score, epochs),
    }));
}


function generateImprovementScore(start_score, dice_1 = true, dice_2 = true, dice_3 = true) {

    let score = start_score.toString();
    let new_score = [];

    [dice_1, dice_2, dice_3].forEach((value, index) => {
        if (value) {
            new_score.push(generateRandomDiceScore());
        } else {
            new_score.push(score[index]);
        }
    });

    return intScoreToFormatedIntScore(parseInt(new_score.join('')));
}

function generateScoreImprovementList(score, score_list, iterations = 1000) {
    const possible_improvements = [
        '001',
        '010',
        '011',
        '100',
        '101',
        '110',
        '111',
    ];

    const results = [];

    improvements_per_possibility_count = iterations;
    const orginal_score_proba = score_list.find((item) => item.score === score).proba;

    possible_improvements.forEach((item) => {

        let count_total_wins = 0;

        for (let i = 0; i < improvements_per_possibility_count; i++) {
            const new_score = generateImprovementScore(score, item[0] == 1, item[1] == 1, item[2] == 1);
            const new_score_proba = score_list.find((item) => item.score === new_score).proba;

            if (new_score_proba > orginal_score_proba) {
                count_total_wins++;
            }
        }

        results.push({
            combination: item,
            proba: count_total_wins / improvements_per_possibility_count,
        })
    });
    
    return results;

}



function generateAllImprovementsForAllScores(score_list) {
    return score_list.map((score) => {
        const improvement_list = generateScoreImprovementList(score.score, score_list)
            .sort((a, b) => a.proba > b.proba ? -1 : 1)
            .filter((item) => item.proba > 0);
        return {
            ...score,
            improvement_list,
        }
    });
}



module.exports = {
    generateRandomScore,
    generateAllPossibilities,
    generateWinProba,
    generateWinProbaAllScores,
    generateImprovementScore,
    generateScoreImprovementList,
    generateAllImprovementsForAllScores,
};
