'use strict';

const {
    saveData,
    generateDataFilepath,
} = require('../libs/DataStorage');

const {
    generateWinProbaAllScores,
} = require('../libs/ProbaGenerator');

const {
    SCORE_LIST_FILENAME,
    SCORE_LIST_EPOCHS,
} = require('configuration');

(async () => {
    await saveData(
        generateDataFilepath(SCORE_LIST_FILENAME),
        generateWinProbaAllScores(SCORE_LIST_EPOCHS)
            .sort((a, b) => a.proba > b.proba ? -1 : 1),
        true
    );
})();
