'use strict';

const {
    SCORE_LIST_FILENAME,
    IMPROVEMENT_SCORE_FILENAME,
} = require('./configuration');

const {
    saveData,
    generateDataFilepath,
    loadData,
    fileExists
} = require('../libs/DataStorage');

const {
    generateAllImprovementsForAllScores,
} = require('../libs/ProbaGenerator');

(async () => {
    if (!fileExists(generateDataFilepath(SCORE_LIST_FILENAME))) {
        console.log("You need to generage win proba score list first");
        process.exit(1);
    }

    const score_list = await loadData(generateDataFilepath(SCORE_LIST_FILENAME));

    await saveData(
        generateDataFilepath(IMPROVEMENT_SCORE_FILENAME),
        generateAllImprovementsForAllScores(score_list),
        true
    );
})();
