module.exports = {
    SCORE_LIST_FILENAME: 'win_proba_score_list.json',
    IMPROVEMENT_SCORE_FILENAME: 'improvement_scores.json',

    SCORE_LIST_EPOCHS: 100000,
    IMPROVEMENT_SCORE_EPOCHS: 1000,
};