const {
    intScoreToFormatedIntScore,
} = require('./libs/ScoreUtilitariesFunctions');

const {
    generateWinProba,
    generateAllPossibilities,
    generateWinProbaAllScores,
    generateImprovementScore,
    generateScoreImprovementList,
    generateAllImprovementsForAllScores,
} = require('./libs/ProbaGenerator');

const SCORE_LIST_FILENAME = 'win_proba_score_list.json',
score_list = await loadData(generateDataFilepath(SCORE_LIST_FILENAME));

console.log(generateAllImprovementsForAllScores(score_list));


