#include <unity.h>
#include <Game.h>

void Game_Game_test_playerRollDices() {
    Game * game = new Game();
    game->initPlayers(1);

    Player * player0 = game->getPlayerByIndex(0);
    player0->setTokens(7);
    
    player0->onPlayEvent([](Player * player) {
        TEST_ASSERT_EQUAL_INT(player->getRoundScore(), 0);
        TEST_ASSERT_EQUAL_INT(player->getTokens(), 7);
        TEST_ASSERT_GREATER_THAN_INT(player->getTokens(), 10);
        player->rollDices(true, true, true);
        TEST_ASSERT_GREATER_THAN_INT(0, player->getRoundScore());
    });

    player0->play();

}

void Game_Game_test_RollDices() {
    Game * game = new Game();
    game->rollDices(true, true, true);
    TEST_ASSERT_GREATER_THAN_INT(0, game->getDiceByIndex(0)->getValue());
    TEST_ASSERT_GREATER_THAN_INT(0, game->getDiceByIndex(1)->getValue());
    TEST_ASSERT_GREATER_THAN_INT(0, game->getDiceByIndex(2)->getValue());
    // std::cout << "Rolling TEST : " << game->getDiceByIndex(0)->getValue() << std::endl;
}


void Game_Game_test() {
    RUN_TEST(Game_Game_test_playerRollDices);
    RUN_TEST(Game_Game_test_RollDices);
}