#include <unity.h>
#include <Game.h>
#include <iostream>


void Game_Game_gameStepDischarge_case1_test() {
    Game * game = new Game();

    game->initPlayers(2);
    game->getPlayerByIndex(0)->setIsFirstInRound(true);


    game->tokens = 0;

    game->setGameState(GAME_STATE_DISCHARGE);

    game->getPlayerByIndex(0)->setTokens(7);
    game->getPlayerByIndex(1)->setTokens(14);

    int count = 0;

    game->getPlayerByIndex(0)->onPlayEvent([&](Player * player) {
        player->setRoundScore(421);
        count++;
        TEST_ASSERT_EQUAL_INT(1, count);
    });

    game->getPlayerByIndex(1)->onPlayEvent([&](Player * player) {
        player->setRoundScore(322);
        count++;
        TEST_ASSERT_EQUAL_INT(2, count);
    });

    game->gameStepDischarge();

    int found_first_player = game->findFirstPlayerIndex();
    TEST_ASSERT_EQUAL_INT(1, found_first_player);
    TEST_ASSERT_EQUAL_INT(0, game->getPlayerByIndex(0)->getTokens());
    TEST_ASSERT_EQUAL_INT(21, game->getPlayerByIndex(1)->getTokens());
}


// 2 rounds chaned with 2 players
void Game_Game_gameStepDischarge_case2_test() {
    Game * game = new Game();

    game->initPlayers(2);
    game->getPlayerByIndex(0)->setIsFirstInRound(true);


    game->tokens = 0;

    game->setGameState(GAME_STATE_DISCHARGE);

    game->getPlayerByIndex(0)->setTokens(10);
    game->getPlayerByIndex(1)->setTokens(11);

    int count = 0;

    game->getPlayerByIndex(0)->onPlayEvent([&](Player * player) {
        count++;

        if (count < 3) {
            TEST_ASSERT_EQUAL_INT(10, player->getTokens());
            TEST_ASSERT_EQUAL_INT(1, count);
            player->setRoundScore(541);
        } else if (count < 5) {
            TEST_ASSERT_EQUAL_INT(9, player->getTokens());
            TEST_ASSERT_EQUAL_INT(4, count);
            player->setRoundScore(421);
        }
    });

    game->getPlayerByIndex(1)->onPlayEvent([&](Player * player) {
        count++;

        if (count < 3) {
            TEST_ASSERT_EQUAL_INT(11, player->getTokens());
            TEST_ASSERT_EQUAL_INT(2, count);
            player->setRoundScore(322);
        } else if (count < 5) {
            TEST_ASSERT_EQUAL_INT(12, player->getTokens());
            TEST_ASSERT_EQUAL_INT(3, count);
            player->setRoundScore(111);
        }
    });

    game->gameStepDischarge();

    int found_first_player = game->findFirstPlayerIndex();
    TEST_ASSERT_EQUAL_INT(found_first_player, 1);
    TEST_ASSERT_EQUAL_INT(game->getPlayerByIndex(0)->getTokens(), 0);
    TEST_ASSERT_EQUAL_INT(game->getPlayerByIndex(1)->getTokens(), 21);
}


// 2 rounds chained with 3 player. One wins on round 1, an other on round 2
void Game_Game_gameStepDischarge_case3_test() {
    Game * game = new Game();

    game->initPlayers(3);
    game->getPlayerByIndex(0)->setIsFirstInRound(true);


    game->tokens = 0;

    game->setGameState(GAME_STATE_DISCHARGE);

    game->getPlayerByIndex(0)->setTokens(10);
    game->getPlayerByIndex(1)->setTokens(7);
    game->getPlayerByIndex(2)->setTokens(4);

    int count = 0;

    game->getPlayerByIndex(0)->onPlayEvent([&](Player * player) {
        count++;

        if (count < 4) {
            TEST_ASSERT_EQUAL_INT(10, player->getTokens());
            TEST_ASSERT_EQUAL_INT(1, count);
            player->setRoundScore(541);
        } else if (count < 6) {
            TEST_ASSERT_EQUAL_INT(10, player->getTokens());
            TEST_ASSERT_EQUAL_INT(5, count);
            player->setRoundScore(421);
        }
    });

    game->getPlayerByIndex(1)->onPlayEvent([&](Player * player) {
        count++;

        if (count < 4) {
            TEST_ASSERT_EQUAL_INT(7, player->getTokens());
            TEST_ASSERT_EQUAL_INT(2, count);
            player->setRoundScore(322);
        } else if (count < 6) {
            TEST_ASSERT_EQUAL_INT(11, player->getTokens());
            TEST_ASSERT_EQUAL_INT(4, count);
            player->setRoundScore(111);
        }
    });

    game->getPlayerByIndex(2)->onPlayEvent([&](Player * player) {
        count++;

        if (count < 4) {
            TEST_ASSERT_EQUAL_INT(4, player->getTokens());
            TEST_ASSERT_EQUAL_INT(3, count);
            player->setRoundScore(421);
        }
    });


    game->gameStepDischarge();

    int found_first_player = game->findFirstPlayerIndex();
    TEST_ASSERT_EQUAL_INT(found_first_player, 1);
    TEST_ASSERT_EQUAL_INT(game->getPlayerByIndex(0)->getTokens(), 0);
    TEST_ASSERT_EQUAL_INT(game->getPlayerByIndex(1)->getTokens(), 21);
    TEST_ASSERT_EQUAL_INT(game->getPlayerByIndex(2)->getTokens(), 0);
}


// 2 rounds chained with 3 player. One wins on round 1, an other on round 2
// Testing Round Callbacks
void Game_Game_gameStepDischarge_case4_test() {
    Game * game = new Game();

    int round_count = 0;

    game->onRoundStartEvent([&](Game * _game) {
        round_count++;
    });


    game->initPlayers(3);
    game->getPlayerByIndex(0)->setIsFirstInRound(true);


    game->tokens = 0;

    game->setGameState(GAME_STATE_DISCHARGE);

    game->getPlayerByIndex(0)->setTokens(10);
    game->getPlayerByIndex(1)->setTokens(7);
    game->getPlayerByIndex(2)->setTokens(4);

    int count = 0;

    game->getPlayerByIndex(0)->onPlayEvent([&](Player * player) {
        count++;

        if (count < 4) {
            TEST_ASSERT_EQUAL_INT(10, player->getTokens());
            TEST_ASSERT_EQUAL_INT(1, count);
            player->setRoundScore(541);
        } else if (count < 6) {
            TEST_ASSERT_EQUAL_INT(10, player->getTokens());
            TEST_ASSERT_EQUAL_INT(5, count);
            player->setRoundScore(421);
        }
    });

    game->getPlayerByIndex(1)->onPlayEvent([&](Player * player) {
        count++;

        if (count < 4) {
            TEST_ASSERT_EQUAL_INT(7, player->getTokens());
            TEST_ASSERT_EQUAL_INT(2, count);
            player->setRoundScore(322);
        } else if (count < 6) {
            TEST_ASSERT_EQUAL_INT(11, player->getTokens());
            TEST_ASSERT_EQUAL_INT(4, count);
            player->setRoundScore(111);
        }
    });

    game->getPlayerByIndex(2)->onPlayEvent([&](Player * player) {
        count++;

        if (count < 4) {
            TEST_ASSERT_EQUAL_INT(4, player->getTokens());
            TEST_ASSERT_EQUAL_INT(3, count);
            player->setRoundScore(421);
        }
    });

    int payment_count = 0;
    game->onPaymentEvent([&](Game * game) {
        payment_count++;
    });

    game->gameStepDischarge();

    int found_first_player = game->findFirstPlayerIndex();

    TEST_ASSERT_EQUAL_INT(2, payment_count);
    TEST_ASSERT_EQUAL_INT(1, found_first_player);
    TEST_ASSERT_EQUAL_INT(0, game->getPlayerByIndex(0)->getTokens());
    TEST_ASSERT_EQUAL_INT(21, game->getPlayerByIndex(1)->getTokens());
    TEST_ASSERT_EQUAL_INT(0, game->getPlayerByIndex(2)->getTokens());

    TEST_ASSERT_EQUAL_INT(2, round_count);
}




void Game_Game_gameStepDischarge_test() {
    RUN_TEST(Game_Game_gameStepDischarge_case1_test);
    RUN_TEST(Game_Game_gameStepDischarge_case2_test);
    RUN_TEST(Game_Game_gameStepDischarge_case3_test);
    RUN_TEST(Game_Game_gameStepDischarge_case4_test);
}