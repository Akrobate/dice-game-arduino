#include <unity.h>
#include <Game.h>


void Game_Game_gameStepDetermineWhoStarts_case1_test() {
    Game * game = new Game();

    game->initPlayers(4);

    game->getPlayerByIndex(0)->onPlayEvent([](Player * player) {
        player->setRoundScore(542);
    });

    game->getPlayerByIndex(1)->onPlayEvent([](Player * player) {
        player->setRoundScore(642);
    });

    game->getPlayerByIndex(2)->onPlayEvent([](Player * player) {
        player->setRoundScore(421);
    });

    game->getPlayerByIndex(3)->onPlayEvent([](Player * player) {
        player->setRoundScore(532);
    });

    game->gameStepDetermineWhoStarts();
    int found_first_player = game->findFirstPlayerIndex();
    TEST_ASSERT_EQUAL_INT(found_first_player, 2);
}


void Game_Game_gameStepDetermineWhoStarts_case2_test() {
    Game * game = new Game();

    game->initPlayers(4);

    game->getPlayerByIndex(0)->onPlayEvent([](Player * player) {
        player->setRoundScore(542);
    });

    game->getPlayerByIndex(1)->onPlayEvent([](Player * player) {
        player->setRoundScore(421);
    });

    game->getPlayerByIndex(2)->onPlayEvent([](Player * player) {
        player->setRoundScore(642);
    });

    game->getPlayerByIndex(3)->onPlayEvent([](Player * player) {
        player->setRoundScore(532);
    });

    game->gameStepDetermineWhoStarts();
    int found_first_player = game->findFirstPlayerIndex();
    TEST_ASSERT_EQUAL_INT(found_first_player, 1);
}


void Game_Game_gameStepDetermineWhoStarts_case3_test() {
    Game * game = new Game();

    game->initPlayers(4);

    game->getPlayerByIndex(0)->onPlayEvent([](Player * player) {
        player->setRoundScore(542);
    });

    game->getPlayerByIndex(1)->onPlayEvent([](Player * player) {
        player->setRoundScore(521);
    });

    game->getPlayerByIndex(2)->onPlayEvent([](Player * player) {
        player->setRoundScore(642);
    });

    game->getPlayerByIndex(3)->onPlayEvent([](Player * player) {
        player->setRoundScore(521);
    });

    game->gameStepDetermineWhoStarts();
    int found_first_player = game->findFirstPlayerIndex();
    TEST_ASSERT_EQUAL_INT(found_first_player, 2);
}


void Game_Game_gameStepDetermineWhoStarts_equality_1_test() {
    Game * game = new Game();

    int player_0_played_count = 0;
    int player_1_played_count = 0;

    game->initPlayers(3);

    game->getPlayerByIndex(0)->onPlayEvent([&](Player * player) {
        switch (player_0_played_count) {
            case 0:
                player->setRoundScore(542);
                break;
            case 1:
                player->setEqualityResolutionScore(611);
                break;
            default:
                player->setRoundScore(0);
        }
        player_0_played_count++;
    });

    game->getPlayerByIndex(1)->onPlayEvent([&](Player * player) {
        switch (player_1_played_count) {
            case 0:
                player->setRoundScore(542);
                break;
            case 1:
                player->setEqualityResolutionScore(421);
                break;
            default:
                player->setRoundScore(0);
        }
        player_1_played_count++;
    });

    game->getPlayerByIndex(2)->onPlayEvent([&](Player * player) {
        player->setRoundScore(322);
    });


    game->gameStepDetermineWhoStarts();

    int found_first_player = game->findFirstPlayerIndex();
    TEST_ASSERT_EQUAL_INT(found_first_player, 1);

    TEST_ASSERT_EQUAL_INT(player_0_played_count, 2);
    TEST_ASSERT_EQUAL_INT(player_1_played_count, 2);
}


void Game_Game_gameStepDetermineWhoStarts_equality_2_test() {
    Game * game = new Game();

    int player_0_played_count = 0;
    int player_1_played_count = 0;

    game->initPlayers(3);

    game->getPlayerByIndex(0)->onPlayEvent([&](Player * player) {
        switch (player_0_played_count) {
            case 0:
                player->setRoundScore(542);
                break;
            case 1:
                player->setEqualityResolutionScore(421);
                break;
            case 2:
                player->setEqualityResolutionScore(611);
                break;
            default:
                player->setRoundScore(0);
        }
        player_0_played_count++;
    });

    game->getPlayerByIndex(1)->onPlayEvent([&](Player * player) {
        switch (player_1_played_count) {
            case 0:
                player->setRoundScore(542);
                break;
            case 1:
                player->setEqualityResolutionScore(421);
                break;
            case 2:
                player->setEqualityResolutionScore(421);
                break;
            default:
                player->setRoundScore(0);
        }
        player_1_played_count++;
    });

    game->getPlayerByIndex(2)->onPlayEvent([&](Player * player) {
        player->setRoundScore(322);
    });


    game->gameStepDetermineWhoStarts();

    int found_first_player = game->findFirstPlayerIndex();
    TEST_ASSERT_EQUAL_INT(found_first_player, 1);

    TEST_ASSERT_EQUAL_INT(player_0_played_count, 3);
    TEST_ASSERT_EQUAL_INT(player_1_played_count, 3);
}



void Game_Game_gameStepDetermineWhoStarts_test() {
    RUN_TEST(Game_Game_gameStepDetermineWhoStarts_case1_test);
    RUN_TEST(Game_Game_gameStepDetermineWhoStarts_case2_test);
    RUN_TEST(Game_Game_gameStepDetermineWhoStarts_case3_test);
    
    RUN_TEST(Game_Game_gameStepDetermineWhoStarts_equality_1_test);
    RUN_TEST(Game_Game_gameStepDetermineWhoStarts_equality_2_test);
}