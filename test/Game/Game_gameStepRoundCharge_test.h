#include <unity.h>
#include <Game.h>
#include <iostream>


void Game_Game_gameStepRoundCharge_case1_test() {
    Game * game = new Game();

    game->initPlayers(3);
    game->getPlayerByIndex(1)->setIsFirstInRound(true);

    int count = 0;

    game->getPlayerByIndex(0)->onPlayEvent([&](Player * player) {
        player->setRoundScore(421);
        count++;
        TEST_ASSERT_EQUAL_INT(count, 3);
    });

    game->getPlayerByIndex(1)->onPlayEvent([&](Player * player) {
        player->setRoundScore(542);
        count++;
        TEST_ASSERT_EQUAL_INT(count, 1);
    });

    game->getPlayerByIndex(2)->onPlayEvent([&](Player * player) {
        player->setRoundScore(322);
        count++;
        TEST_ASSERT_EQUAL_INT(count, 2);
    });

    int payments_count = 0;
    game->onPaymentEvent([&](Game * game) {
        payments_count++;
        TEST_ASSERT_EQUAL_INT(-1, game->payment_player_index_from);
        TEST_ASSERT_EQUAL_INT(2, game->payment_player_index_to);
        TEST_ASSERT_EQUAL_INT(10, game->payment_tokens_amount);
    });


    game->setGameState(GAME_STATE_CHARGE);
    game->gameStepRoundCharge();

    int found_first_player = game->findFirstPlayerIndex();
    TEST_ASSERT_EQUAL_INT(payments_count, 1);
    TEST_ASSERT_EQUAL_INT(found_first_player, 2);
    TEST_ASSERT_EQUAL_INT(game->tokens, 21 - 10);
    TEST_ASSERT_EQUAL_INT(game->getPlayerByIndex(0)->getTokens(), 0);
    TEST_ASSERT_EQUAL_INT(game->getPlayerByIndex(1)->getTokens(), 0);
    TEST_ASSERT_EQUAL_INT(game->getPlayerByIndex(2)->getTokens(), 10);
}


void Game_Game_gameStepRoundCharge_case2_test() {
    Game * game = new Game();

    game->initPlayers(3);
    game->tokens = 2;
    game->getPlayerByIndex(1)->setIsFirstInRound(true);

    int count = 0;

    game->getPlayerByIndex(0)->onPlayEvent([&](Player * player) {
        player->setRoundScore(111);
        count++;
        TEST_ASSERT_EQUAL_INT(count, 3);
    });

    game->getPlayerByIndex(1)->onPlayEvent([&](Player * player) {
        player->setRoundScore(542);
        count++;
        TEST_ASSERT_EQUAL_INT(count, 1);
    });

    game->getPlayerByIndex(2)->onPlayEvent([&](Player * player) {
        player->setRoundScore(322);
        count++;
        TEST_ASSERT_EQUAL_INT(count, 2);
    });
    
    game->setGameState(GAME_STATE_CHARGE);
    game->gameStepRoundCharge();

    int found_first_player = game->findFirstPlayerIndex();
    TEST_ASSERT_EQUAL_INT(found_first_player, 2);
    TEST_ASSERT_EQUAL_INT(game->tokens, 0);
    TEST_ASSERT_EQUAL_INT(game->getPlayerByIndex(0)->getTokens(), 0);
    TEST_ASSERT_EQUAL_INT(game->getPlayerByIndex(1)->getTokens(), 0);
    TEST_ASSERT_EQUAL_INT(game->getPlayerByIndex(2)->getTokens(), 2);
}

// Best score equality should not enter in equality resolution
void Game_Game_gameStepRoundCharge_case3_test() {
    Game * game = new Game();

    game->initPlayers(3);
    game->getPlayerByIndex(1)->setIsFirstInRound(true);

    int count = 0;

    game->getPlayerByIndex(0)->onPlayEvent([&](Player * player) {
        player->setRoundScore(421);
        count++;
        TEST_ASSERT_EQUAL_INT(count, 3);
    });

    game->getPlayerByIndex(1)->onPlayEvent([&](Player * player) {
        player->setRoundScore(421);
        count++;
        TEST_ASSERT_EQUAL_INT(count, 1);
    });

    game->getPlayerByIndex(2)->onPlayEvent([&](Player * player) {
        player->setRoundScore(322);
        count++;
        TEST_ASSERT_EQUAL_INT(count, 2);
    });

    game->setGameState(GAME_STATE_CHARGE);
    game->gameStepRoundCharge();

    int found_first_player = game->findFirstPlayerIndex();
    TEST_ASSERT_EQUAL_INT(found_first_player, 2);
    TEST_ASSERT_EQUAL_INT(game->tokens, 21 - 10);
    TEST_ASSERT_EQUAL_INT(game->getPlayerByIndex(0)->getTokens(), 0);
    TEST_ASSERT_EQUAL_INT(game->getPlayerByIndex(1)->getTokens(), 0);
    TEST_ASSERT_EQUAL_INT(game->getPlayerByIndex(2)->getTokens(), 10);
}


void Game_Game_gameStepRoundCharge_test() {
    RUN_TEST(Game_Game_gameStepRoundCharge_case1_test);
    RUN_TEST(Game_Game_gameStepRoundCharge_case2_test);
    RUN_TEST(Game_Game_gameStepRoundCharge_case3_test);
}