#include <unity.h>
#include <Game.h>
#include <iostream>


void Game_Game_gameStepRoundDischarge_case1_test() {
    Game * game = new Game();

    game->initPlayers(3);
    game->getPlayerByIndex(1)->setIsFirstInRound(true);

    game->tokens = 0;
    game->setGameState(GAME_STATE_DISCHARGE);

    game->getPlayerByIndex(0)->setTokens(6);
    game->getPlayerByIndex(1)->setTokens(10);
    game->getPlayerByIndex(2)->setTokens(5);

    int count = 0;

    game->getPlayerByIndex(0)->onPlayEvent([&](Player * player) {
        player->setRoundScore(421);
        count++;
        TEST_ASSERT_EQUAL_INT(count, 3);
    });

    game->getPlayerByIndex(1)->onPlayEvent([&](Player * player) {
        player->setRoundScore(542);
        count++;
        TEST_ASSERT_EQUAL_INT(count, 1);
    });

    game->getPlayerByIndex(2)->onPlayEvent([&](Player * player) {
        player->setRoundScore(322);
        count++;
        TEST_ASSERT_EQUAL_INT(count, 2);
    });

    int payments_count = 0;

    game->onPaymentEvent([&](Game * game) {
        payments_count++;
        TEST_ASSERT_EQUAL_INT(0, game->payment_player_index_from);
        TEST_ASSERT_EQUAL_INT(2, game->payment_player_index_to);
        TEST_ASSERT_EQUAL_INT(6, game->payment_tokens_amount);
    });


    game->gameStepRoundDischarge();

    int found_first_player = game->findFirstPlayerIndex();

    TEST_ASSERT_EQUAL_INT(1, payments_count);
    
    TEST_ASSERT_EQUAL_INT(found_first_player, 2);
    TEST_ASSERT_EQUAL_INT(game->tokens, 0);
    TEST_ASSERT_EQUAL_INT(0, game->getPlayerByIndex(0)->getTokens());
    TEST_ASSERT_EQUAL_INT(10, game->getPlayerByIndex(1)->getTokens());
    TEST_ASSERT_EQUAL_INT(11, game->getPlayerByIndex(2)->getTokens());

    TEST_ASSERT_TRUE(game->getPlayerByIndex(0)->getGameWon());
    TEST_ASSERT_FALSE(game->getPlayerByIndex(1)->getGameWon());
    TEST_ASSERT_FALSE(game->getPlayerByIndex(2)->getGameWon());
}



void Game_Game_gameStepRoundDischarge_case2_test() {
    Game * game = new Game();

    game->initPlayers(3);
    game->getPlayerByIndex(2)->setIsFirstInRound(true);

    game->tokens = 0;
    game->setGameState(GAME_STATE_DISCHARGE);

    game->getPlayerByIndex(0)->setTokens(16);
    game->getPlayerByIndex(1)->setTokens(2);
    game->getPlayerByIndex(2)->setTokens(3);

    int count = 0;

    game->getPlayerByIndex(0)->onPlayEvent([&](Player * player) {
        player->setRoundScore(111);
        count++;
        TEST_ASSERT_EQUAL_INT(count, 2);
    });

    game->getPlayerByIndex(1)->onPlayEvent([&](Player * player) {
        player->setRoundScore(542);
        count++;
        TEST_ASSERT_EQUAL_INT(count, 3);
    });

    game->getPlayerByIndex(2)->onPlayEvent([&](Player * player) {
        player->setRoundScore(322);
        count++;
        TEST_ASSERT_EQUAL_INT(count, 1);
    });

    game->gameStepRoundDischarge();

    int found_first_player = game->findFirstPlayerIndex();
    TEST_ASSERT_EQUAL_INT(found_first_player, 2);
    TEST_ASSERT_EQUAL_INT(game->tokens, 0);
    TEST_ASSERT_EQUAL_INT(game->getPlayerByIndex(0)->getTokens(), 9);
    TEST_ASSERT_EQUAL_INT(game->getPlayerByIndex(1)->getTokens(), 2);
    TEST_ASSERT_EQUAL_INT(game->getPlayerByIndex(2)->getTokens(), 10);

    TEST_ASSERT_FALSE(game->getPlayerByIndex(0)->getGameWon());
    TEST_ASSERT_FALSE(game->getPlayerByIndex(1)->getGameWon());
    TEST_ASSERT_FALSE(game->getPlayerByIndex(2)->getGameWon());
}


void Game_Game_gameStepRoundDischarge_test() {
    RUN_TEST(Game_Game_gameStepRoundDischarge_case1_test);
    RUN_TEST(Game_Game_gameStepRoundDischarge_case2_test);
}