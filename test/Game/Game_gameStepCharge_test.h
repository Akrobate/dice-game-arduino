#include <unity.h>
#include <Game.h>
#include <iostream>

// @Todo : work in progress
// Program received signal SIGSEGV
void Game_Game_gameStepCharge_case1_test() {
    Game * game = new Game();

    game->setTokens(10);

    int player_0_played_count = 0;
    int player_1_played_count = 0;

    game->initPlayers(2);
    game->setGameState(GAME_STATE_CHARGE);
    game->getPlayerByIndex(0)->setIsFirstInRound(true);

    game->getPlayerByIndex(0)->onPlayEvent([&](Player * player) {
        player_0_played_count++;

        switch (player_0_played_count) {
            // ROund 1
            case 1:
                player->setRoundScore(541);
                break;
            case 2:
                player->setRoundScore(431);
                break;
            default:
                player->setRoundScore(0);

            
        }
    });

    game->getPlayerByIndex(1)->onPlayEvent([&](Player * player) {
        player_1_played_count++;

        switch (player_1_played_count) {
            // ROund 1
            case 1:
                player->setRoundScore(542);
                break;
            case 2:
                player->setRoundScore(421);
                break;
            default:
                player->setRoundScore(0);
        }
        
    });


    TEST_ASSERT_EQUAL_INT(0, game->getPlayerByIndex(1)->getTokens());
    TEST_ASSERT_EQUAL_INT(0, game->getPlayerByIndex(0)->getTokens());
    TEST_ASSERT_EQUAL_INT(10, game->getTokens());


    game->gameStepCharge();


    int found_first_player = game->findFirstPlayerIndex();
    TEST_ASSERT_EQUAL_INT(found_first_player, 0);

    TEST_ASSERT_EQUAL_INT(0, game->getTokens());

    TEST_ASSERT_EQUAL_INT(2, player_0_played_count);
    TEST_ASSERT_EQUAL_INT(2, player_1_played_count);

    TEST_ASSERT_EQUAL_INT(10, game->getPlayerByIndex(0)->getTokens());
    TEST_ASSERT_EQUAL_INT(0, game->getPlayerByIndex(1)->getTokens());
    TEST_ASSERT_EQUAL_INT(0, game->getTokens());
    TEST_ASSERT_TRUE(game->getPlayerByIndex(1)->getGameWon());
}


void Game_Game_gameStepCharge_test() {
    RUN_TEST(Game_Game_gameStepCharge_case1_test);
}
