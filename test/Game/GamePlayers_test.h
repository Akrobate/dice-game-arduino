#include <unity.h>
#include <Game.h>

int eval2PlayersGameWinner(int score1, int score2) {
    Game * game = new Game();
    game->initPlayers(2);
    game->getPlayerByIndex(0)->setRoundScore(score1);
    game->getPlayerByIndex(1)->setRoundScore(score2);
    return game->findRoundWinnerPlayerIndex();
}

int eval3PlayersGameWinner(int score1, int score2, int score3) {
    Game * game = new Game();
    game->initPlayers(3);
    game->getPlayerByIndex(0)->setRoundScore(score1);
    game->getPlayerByIndex(1)->setRoundScore(score2);
    game->getPlayerByIndex(2)->setRoundScore(score3);
    return game->findRoundWinnerPlayerIndex();
}

int eval2PlayersGameLoser(int score1, int score2) {
    Game * game = new Game();
    game->initPlayers(2);
    game->getPlayerByIndex(0)->setRoundScore(score1);
    game->getPlayerByIndex(1)->setRoundScore(score2);
    return game->findRoundLoserPlayerIndex();
}

int eval3PlayersGameLoser(int score1, int score2, int score3) {
    Game * game = new Game();
    game->initPlayers(3);
    game->getPlayerByIndex(0)->setRoundScore(score1);
    game->getPlayerByIndex(1)->setRoundScore(score2);
    game->getPlayerByIndex(2)->setRoundScore(score3);
    return game->findRoundLoserPlayerIndex();
}

void GamePlayers_Game_test_findWinner_2_players() {
    TEST_ASSERT_EQUAL_INT(eval2PlayersGameWinner(421, 321), 0);
    TEST_ASSERT_EQUAL_INT(eval2PlayersGameWinner(321, 421), 1);
    TEST_ASSERT_EQUAL_INT(eval2PlayersGameWinner(421, 421), -1);
    TEST_ASSERT_EQUAL_INT(eval2PlayersGameWinner(541, 322), 0);
    TEST_ASSERT_EQUAL_INT(eval2PlayersGameWinner(222, 211), 1);
    TEST_ASSERT_EQUAL_INT(eval2PlayersGameWinner(222, 654), 0);
    TEST_ASSERT_EQUAL_INT(eval2PlayersGameWinner(661, 221), 0);
    TEST_ASSERT_EQUAL_INT(eval2PlayersGameWinner(553, 553), -1);
}

void GamePlayers_Game_test_findLoser_2_players() {
    TEST_ASSERT_EQUAL_INT(eval2PlayersGameLoser(421, 321), 1);
    TEST_ASSERT_EQUAL_INT(eval2PlayersGameLoser(321, 421), 0);
    TEST_ASSERT_EQUAL_INT(eval2PlayersGameLoser(421, 421), -1);
    TEST_ASSERT_EQUAL_INT(eval2PlayersGameLoser(541, 322), 1);
    TEST_ASSERT_EQUAL_INT(eval2PlayersGameLoser(222, 211), 0);
    TEST_ASSERT_EQUAL_INT(eval2PlayersGameLoser(222, 654), 1);
    TEST_ASSERT_EQUAL_INT(eval2PlayersGameLoser(661, 221), 1);
    TEST_ASSERT_EQUAL_INT(eval2PlayersGameLoser(553, 553), -1);
}

void GamePlayers_Game_test_findWinner_3_players() {
    TEST_ASSERT_EQUAL_INT(eval3PlayersGameWinner(431, 321, 421), 2);
    TEST_ASSERT_EQUAL_INT(eval3PlayersGameWinner(666, 421, 111), 1);
    TEST_ASSERT_EQUAL_INT(eval3PlayersGameWinner(651, 631, 431), 0);
    TEST_ASSERT_EQUAL_INT(eval3PlayersGameWinner(421, 421, 111), -1);
}

void GamePlayers_Game_test_findLoser_3_players() {
    TEST_ASSERT_EQUAL_INT(eval3PlayersGameLoser(431, 321, 421), 0);
    TEST_ASSERT_EQUAL_INT(eval3PlayersGameLoser(666, 421, 111), 0);
    TEST_ASSERT_EQUAL_INT(eval3PlayersGameLoser(651, 631, 431), 2);
    TEST_ASSERT_EQUAL_INT(eval3PlayersGameLoser(421, 111, 111), -1);
}


void GamePlayers_Game_test_findFirstPlayerIndex() {
    Game * game = new Game();
    game->initPlayers(4);
    game->getPlayerByIndex(2)->setIsFirstInRound(true);
    int found_first_player = game->findFirstPlayerIndex();
    TEST_ASSERT_EQUAL_INT(found_first_player, 2);
}



void GamePlayers_Game_test_initPlayers() {
    Game * game = new Game();

    game->initPlayers(2);

    game->getPlayerByIndex(0)->setTokens(7);
    game->getPlayerByIndex(1)->setTokens(1);

    Player * player0 = game->getPlayerByIndex(0);
    Player * player1 = game->getPlayerByIndex(1);


    player0->onPlayEvent([](Player * player) {
        TEST_ASSERT_EQUAL_INT(player->getTokens(), 7);
        player->setTokens(13);
    });

    game->players[0].play();
    TEST_ASSERT_EQUAL_INT(player0->getTokens(), 13);
    TEST_ASSERT_EQUAL_INT(player0->getPlayerIndexInGame(), 0);
    TEST_ASSERT_EQUAL_INT(player1->getPlayerIndexInGame(), 1);
}





/**
 * Test entry point
 */
void GamePlayers_Game_test() {
    RUN_TEST(GamePlayers_Game_test_findWinner_2_players);
    RUN_TEST(GamePlayers_Game_test_findLoser_2_players);
    RUN_TEST(GamePlayers_Game_test_findWinner_3_players);
    RUN_TEST(GamePlayers_Game_test_findLoser_3_players);
    RUN_TEST(GamePlayers_Game_test_findFirstPlayerIndex);

    RUN_TEST(GamePlayers_Game_test_initPlayers);
}