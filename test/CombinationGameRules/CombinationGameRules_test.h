#include <unity.h>
#include <CombinationGameRules.h>

void CombinationGameRules_CombinationGameRules_test_findCombinationIndexByScore() {
    CombinationGameRules * combination_game_rules = CombinationGameRules::getInstance();
    TEST_ASSERT_EQUAL_INT(combination_game_rules->getCombinationByIndex(combination_game_rules->findCombinationIndexByScore(421)).value, 421);
    TEST_ASSERT_EQUAL_INT(combination_game_rules->getCombinationByIndex(combination_game_rules->findCombinationIndexByScore(611)).value, 611);
    TEST_ASSERT_EQUAL_INT(combination_game_rules->getCombinationByIndex(combination_game_rules->findCombinationIndexByScore(321)).value, 321);
    TEST_ASSERT_EQUAL_INT(combination_game_rules->getCombinationByIndex(combination_game_rules->findCombinationIndexByScore(111)).value, 111);
}


void CombinationGameRules_CombinationGameRules_test_getTokensCountByScore() {
    CombinationGameRules * combination_game_rules = CombinationGameRules::getInstance();
    TEST_ASSERT_EQUAL_INT(combination_game_rules->getTokensCountByScore(421), 10);
    TEST_ASSERT_EQUAL_INT(combination_game_rules->getTokensCountByScore(111), 7);
    TEST_ASSERT_EQUAL_INT(combination_game_rules->getTokensCountByScore(211), 2);
    TEST_ASSERT_EQUAL_INT(combination_game_rules->getTokensCountByScore(311), 3);
    TEST_ASSERT_EQUAL_INT(combination_game_rules->getTokensCountByScore(322), 1);
    TEST_ASSERT_EQUAL_INT(combination_game_rules->getTokensCountByScore(431), 1);
    TEST_ASSERT_EQUAL_INT(combination_game_rules->getTokensCountByScore(553), 1);
}


void CombinationGameRules_CombinationGameRules_test_dicesValuesToInt() {
    CombinationGameRules * combination_game_rules = CombinationGameRules::getInstance();
    TEST_ASSERT_EQUAL_INT(combination_game_rules->dicesValuesToInt(1, 3, 5), 531);
    TEST_ASSERT_EQUAL_INT(combination_game_rules->dicesValuesToInt(2, 6, 5), 652);
    TEST_ASSERT_EQUAL_INT(combination_game_rules->dicesValuesToInt(5, 5, 5), 555);
    TEST_ASSERT_EQUAL_INT(combination_game_rules->dicesValuesToInt(1, 5, 5), 551);
    TEST_ASSERT_EQUAL_INT(combination_game_rules->dicesValuesToInt(1, 2, 4), 421);
    TEST_ASSERT_EQUAL_INT(combination_game_rules->dicesValuesToInt(1, 1, 6), 611);
}



void CombinationGameRules_CombinationGameRules_test() {
    RUN_TEST(CombinationGameRules_CombinationGameRules_test_findCombinationIndexByScore);
    RUN_TEST(CombinationGameRules_CombinationGameRules_test_getTokensCountByScore);
    RUN_TEST(CombinationGameRules_CombinationGameRules_test_dicesValuesToInt);
}