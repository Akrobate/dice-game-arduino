#include <unity.h>
#include <ImprovementsDiceScore.h>

void ImprovementsDiceScore_ImprovementsDiceScore_test_resetPossibleImprovements() {
    ImprovementsDiceScore * improvements_dice_score = new ImprovementsDiceScore();
    improvements_dice_score->resetPossibleImprovements();
    TEST_ASSERT_EQUAL_INT(ImprovementsDiceScore::IMPROVEMENT_COUNT, 10);
    for(int i = 0; i < ImprovementsDiceScore::IMPROVEMENT_COUNT; i++) {
        TEST_ASSERT_EQUAL_INT(
            0,
            improvements_dice_score->improvement_list[i].target_score
        );
    }
}

void ImprovementsDiceScore_ImprovementsDiceScore_test() {
    RUN_TEST(ImprovementsDiceScore_ImprovementsDiceScore_test_resetPossibleImprovements);
}