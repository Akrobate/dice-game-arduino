#include <unity.h>
#include <Player.h>


void Player_Player_test_staticMembersTest() {
    TEST_ASSERT_EQUAL_INT(Player::ROUND_SCORE, 1);
    TEST_ASSERT_EQUAL_INT(Player::EQUALITY_RESOLUTION_SCORE, 2);
}


void Player_Player_test_getScore() {
    Player * player = new Player();
    player->setRoundScore(321);
    player->setEqualityResolutionScore(541);
    TEST_ASSERT_EQUAL_INT(player->getScore(Player::ROUND_SCORE), 321);
    TEST_ASSERT_EQUAL_INT(player->getScore(Player::EQUALITY_RESOLUTION_SCORE), 541);
}


void Player_Player_test() {
    RUN_TEST(Player_Player_test_staticMembersTest);
    RUN_TEST(Player_Player_test_getScore);
}