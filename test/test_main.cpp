// https://www.geeksforgeeks.org/generics-in-c/

#include <iostream>
#include <unity.h>

#ifdef UNIT_TEST
    #include "ArduinoFake.h"
#else
    #include "Arduino.h"
#endif

#include "./Game/Game_test.h"
#include "./CombinationGameRules/CombinationGameRules_test.h"
#include "./Game/GamePlayers_test.h"
#include "./Game/Game_gameStepDetermineWhoStarts_test.h"
#include "./Game/Game_gameStepRoundCharge_test.h"
#include "./Game/Game_gameStepCharge_test.h"
#include "./Game/Game_gameStepRoundDischarge_test.h"
#include "./Game/Game_gameStepDischarge_test.h"
#include "./Player/Player_test.h"
#include "./PlayerBrain/PlayerBrain_basic_rules_test.h"
#include "./ImprovementsDiceScore/ImprovementsDiceScore_test.h"

int main(int argc, char **argv) {
    srand(time(NULL));

    UNITY_BEGIN();
    CombinationGameRules_CombinationGameRules_test();
    Game_Game_test();
    GamePlayers_Game_test();
    Player_Player_test();
    Game_Game_gameStepDetermineWhoStarts_test();
    Game_Game_gameStepRoundCharge_test();
    Game_Game_gameStepCharge_test();
    Game_Game_gameStepRoundDischarge_test();
    Game_Game_gameStepDischarge_test();
    PlayerBrain_PlayerBrain_basic_rules_test();
    ImprovementsDiceScore_ImprovementsDiceScore_test();
    UNITY_END();
}
