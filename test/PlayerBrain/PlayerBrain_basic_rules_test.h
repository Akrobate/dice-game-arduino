#include <unity.h>
#include <PlayerBrain.h>

// If is first dice roll play always
void PlayerBrain_PlayerBrain_basic_rules_test_1() {
    PlayerBrain * player_brain = new PlayerBrain();

    player_brain->setLaunchesCount(0);
    player_brain->makeDecision();

    TEST_ASSERT_TRUE(player_brain->getRollDices());
    TEST_ASSERT_TRUE(player_brain->roll_dice_1);
    TEST_ASSERT_TRUE(player_brain->roll_dice_2);
    TEST_ASSERT_TRUE(player_brain->roll_dice_3);
}

// Should not play if is best score or get worst score



void PlayerBrain_PlayerBrain_basic_rules_test() {
    RUN_TEST(PlayerBrain_PlayerBrain_basic_rules_test_1);
}